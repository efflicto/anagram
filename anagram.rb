require_relative "Arrangement_options"
require "time"

class Anagramm

  anagrams=[]

  def get_words(wordlist)
    words = []
    f = File.open(wordlist, "r")
    f.each_line do |line|

      word = line.downcase
      word = word.gsub("\n", "")
      words.push(word)
    end
    f.close
    return words
  end

  def get_anagrams(word, wordlist)
    m = Arrangement_options.new
    ana = []
    i = m.get_arr_opts(word)
    while i != 0 do
      shffl = word.split("").shuffle.join
      if ana.include?(shffl.downcase)
      else
        ana.push(shffl.downcase)
        puts(shffl.downcase)
        i-=1
      end
    end
    words = get_words(wordlist)
    anagrams=[]
    ana.each do |x|
      if words.include?(x)
        anagrams.push(x)
      end
    end
    return anagrams
  end

end

