require_relative "anagramm"

x=Anagramm.new

starttime = Time.now
var = x.get_anagrams("lagerregal", "german_words.txt")
stoptime = Time.now
puts "Got #{var.count} anagrams in #{(stoptime - starttime)*1000} ms"
print var