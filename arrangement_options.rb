class Arrangement_options

  def fac(inp)
    (1..(inp.zero? ? 1 : inp)).inject(:*)
  end
  def char_count(str)
    char_counts={}
    str.each_char do |x|
      if char_counts.has_key?(x)
        char_counts[x]+=1
      else
        char_counts[x]=1
      end
    end
    return char_counts
  end
  def word_length(str)
    str.length
  end

  def fac_from_chars(h)
    arr_fac=[]
    h.each do |key, value|
      arr_fac.push(fac(value))
    end
    arr_fac.reject(&:zero?).inject(:*)
  end
  def get_arr_opts(word)
    word_fac = fac(word_length(word))
    char_fac = fac_from_chars(char_count(word))
    word_fac/char_fac
  end

end